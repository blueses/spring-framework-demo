package ioc003;

import org.springframework.stereotype.Component;

@Component
public class User003 {
    private int id = 3;
    private String name = "Jac003";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
