package ioc001;

import org.springframework.stereotype.Component;

@Component
public class User001 {
    private int id = 1;
    private String name = "Jack001";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
