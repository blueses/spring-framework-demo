package ioc001;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main001 {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(Ioc001Config.class);
        User001 user001 = applicationContext.getBean(User001.class);
        System.out.println(user001.getId());
        System.out.println(user001.getName());
    }
}
