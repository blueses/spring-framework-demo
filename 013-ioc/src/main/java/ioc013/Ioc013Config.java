package ioc013;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Ioc013Config {

    @Bean
    @Conditional(User013Condition.class)
    public User013 user013() {
        return new User013();
    }


}
