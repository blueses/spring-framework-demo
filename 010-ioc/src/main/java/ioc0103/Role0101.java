package ioc0103;

import ioc0101.User0101;
import ioc0102.User0102;

public class Role0101 {

    private User0101 user0101;
    private User0102 user0102;

    public Role0101(User0101 user0101, User0102 user0102) {
        this.user0101 = user0101;
        this.user0102 = user0102;
    }

    public User0101 getUser0101() {
        return user0101;
    }

    public void setUser0101(User0101 user0101) {
        this.user0101 = user0101;
    }

    public User0102 getUser0102() {
        return user0102;
    }

    public void setUser0102(User0102 user0102) {
        this.user0102 = user0102;
    }
}
