package ioc0101;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Ioc0101Config {

    @Bean
    public User0101 user0101(){
        return new User0101("0101","Jack0101");
    }
}
