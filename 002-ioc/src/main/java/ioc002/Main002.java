package ioc002;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main002 {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-ioc002.xml");
        User002 user = applicationContext.getBean(User002.class);
        System.out.println(user.getId());
        System.out.println(user.getName());
    }
}
