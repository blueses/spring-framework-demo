package ioc002;

import org.springframework.stereotype.Component;

@Component
public class User002 {
    private int id = 2;
    private String name = "Jack002";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
