package ioc016;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class User016 {

    private String id;
    private Nums016 nums016;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Nums016 getNums016() {
        return nums016;
    }

    @Autowired
    @Qualifier("one016")
    public void setNums016(Nums016 nums016) {
        this.nums016 = nums016;
    }
}
