package ioc016;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("two016")
public class Two016 implements Nums016 {
    @Override
    public void say() {
        System.out.println("222");
    }
}
