package ioc020;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-ioc020.xml")
public class IocXML020Test {

    @Autowired
    private Role0204 role0204;

    @Test
    public void testUser(){
        role0204.say();
    }

    @Autowired
    private Role0205 role0205;

    @Test
    public void testRole0202(){
        role0205.say();
    }
}
