package ioc020;

public class Role0205 {

    public String name;

    public Role0205(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void say(){
        System.out.println(name);
    }
}
