package ioc020;

public class Role0204 {

    public String name;

    public Role0204(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void say(){
        System.out.println(name);
    }
}
