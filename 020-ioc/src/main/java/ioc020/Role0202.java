package ioc020;

import com.alibaba.fastjson.JSON;

import java.util.List;

public class Role0202 {

    public String name;
    public List<String> titles;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public void say(){
        System.out.println(JSON.toJSONString(titles));
    }
}
