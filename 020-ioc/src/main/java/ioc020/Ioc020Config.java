package ioc020;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.*;
import org.springframework.core.io.ClassPathResource;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class Ioc020Config {

    @Value("#{T(System).currentTimeMillis()}")
    private long time;

    @Bean
    public User020 user020(){
        User020 user020 = new User020();
        user020.setId("020");
        user020.setName("user020");
        user020.setTime(time);
        return user020;
    }

    @Bean
    public Role0201 role0201(){
        Role0201 role0201 = new Role0201();

        role0201.setName("role0201");

        List<String> titles = new ArrayList<String>(){{
            add("title02011");
            add("title02012");
            add("title02013");
        }};
        role0201.setTitles(titles);

        return role0201;
    }


    @Bean
    public Role0202 role0202(@Value("#{role0201.titles}") List<String> titles){
        Role0202 role0202 = new Role0202();
        role0202.setName("role0201");
        role0202.setTitles(titles);
        return role0202;
    }

    @Value("#{systemProperties['os.name']}")
    private String osName;


    @Bean
    public PropertiesFactoryBean ioc020Properties(){
        PropertiesFactoryBean app020 = new PropertiesFactoryBean();
        //可以添加多个
        app020.setLocations(new ClassPathResource("app020.properties"));
        return app020;
    }

    @Bean
    public Role0203 role0203(@Value("#{ioc020Properties['app020.role.name']}") String name){
        Role0203 role0203 = new Role0203();
        role0203.setName(name);
        return role0203;
    }
}

