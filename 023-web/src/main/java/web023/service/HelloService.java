package web023.service;

import org.springframework.stereotype.Service;
import web023.model.User;

import java.util.ArrayList;
import java.util.List;

@Service
public class HelloService {

    public List<User> changeName(String name) {
        List<User> users = new ArrayList<User>();
        users.add(new User(1, name + "1"));
        users.add(new User(2, name + "2"));
        return users;
    }

}


