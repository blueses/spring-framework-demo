package aop021;


import aop021.service.AopService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Aop021Config.class})
public class Aop021Test {

    @Autowired
    private AopService aopService;


    @Test
    public void testHello() {
        aopService.hello();
    }

    @Test
    public void testCount() {
        aopService.count(99);
    }
}



