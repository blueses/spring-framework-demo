package aop021.service;

/**
 * @author guoyibin
 */
public class AopService {

    public void hello() {
        System.out.println("hello ===============");
    }

    public void count(int num) {
        System.out.println("count num=[" + num + "] ===============");
    }
}


