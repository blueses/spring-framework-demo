package aop021;

import aop021.service.AopService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @author guoyibin
 */
@Configuration
@EnableAspectJAutoProxy
public class Aop021Config {

    @Bean
    public Audience audience(){
        return new Audience();
    }

    @Bean
    public AopService aopService(){
        return new AopService();
    }
}