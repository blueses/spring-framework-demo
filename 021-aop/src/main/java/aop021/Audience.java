package aop021;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

/**
 * @author guoyibin
 */
@Aspect
public class Audience {
    /**
     * 定义切点
     */
    @Pointcut("execution(* aop021.service.AopService.hello(..))")
    public void performance() {
    }


    /**
     * 方法执行前
     */
    @Before("performance()")
    public void before() {
        System.out.println("before performance .......");
    }

    /**
     * 方法返回后
     */
    @AfterReturning("performance()")
    public void afterReturn() {
        System.out.println("after return performance .......");
    }

    /**
     * 方法抛出异常后
     */
    @AfterThrowing("performance()")
    public void afterThrow() {
        System.out.println("after throw performance .......");
    }

    @Around("performance()")
    public void around(ProceedingJoinPoint point) {
        try {
            System.out.println("around before performance .......");
            point.proceed();
            System.out.println("around after performance .......");
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    @Pointcut("execution(* aop021.service.AopService.count(int)) && args(num)")
    public void performanceArgs(int num) {
    }


    @After("performanceArgs(num)")
    public void after(int num) {
        System.out.println("after performanceArgs num=[" + num + "] .......");
    }

}
