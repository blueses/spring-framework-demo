package ioc007;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Ioc007Config {


    @Bean
    public User0071 user0071(){
        return new User0071();
    }

    @Bean(name = "helloUser0072")
    public User0072 user0072(){
        return new User0072();
    }

    @Bean
    public Role0071 role0071(){
        return new Role0071(user0071());
    }

    @Bean
    public Role0072 role0072(User0071 user0071){
        return new Role0072(user0071);
    }

    @Bean
    public Role0073 role0073(User0071 user0071){
        Role0073 role0073 = new Role0073();
        role0073.setUser0071(user0071);
        return role0073;
    }

}
