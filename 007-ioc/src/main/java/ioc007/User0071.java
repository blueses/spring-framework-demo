package ioc007;


public class User0071 {
    private int id = 7;
    private String name = "Jac0071";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
