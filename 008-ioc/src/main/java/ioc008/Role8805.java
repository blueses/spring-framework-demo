package ioc008;

public class Role8805 {

    private User0082 user0082;

    public Role8805() {}

    public Role8805(User0082 user0082) {
        this.user0082 = user0082;
    }

    public User0082 getUser0082() {
        return user0082;
    }

    public void setUser0082(User0082 user0082) {
        this.user0082 = user0082;
    }

    public void say(){
        System.out.println("包含用户："+user0082.getName());
    }
}
