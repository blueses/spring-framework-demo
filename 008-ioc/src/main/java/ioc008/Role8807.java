package ioc008;

import java.util.List;

public class Role8807 {

    private User0082 user0082;

    private String name;

    private List<String> titles;

    public User0082 getUser0082() {
        return user0082;
    }

    public void setUser0082(User0082 user0082) {
        this.user0082 = user0082;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public void say(){
        System.out.println("角色"+name+"包含用户："+user0082.getName()+",包含第一个标题是："+titles.get(0));
    }
}
