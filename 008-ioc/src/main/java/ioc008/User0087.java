package ioc008;

import java.util.List;

public class User0087 {

    private String id;
    private String name;
    private List<String> titles;

    public User0087(String id, String name, List<String> titles) {
        this.id = id;
        this.name = name;
        this.titles = titles;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }
}
