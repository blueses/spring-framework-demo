package ioc008;

import java.util.Set;

public class User0089 {

    private String id;
    private String name;
    private Set<String> titles;

    public User0089(String id, String name, Set<String> titles) {
        this.id = id;
        this.name = name;
        this.titles = titles;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<String> getTitles() {
        return titles;
    }

    public void setTitles(Set<String> titles) {
        this.titles = titles;
    }
}
