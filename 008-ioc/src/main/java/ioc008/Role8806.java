package ioc008;

public class Role8806 {

    private User0082 user0082;

    public Role8806() {}

    public Role8806(User0082 user0082) {
        this.user0082 = user0082;
    }

    public User0082 getUser0082() {
        return user0082;
    }

    public void setUser0082(User0082 user0082) {
        this.user0082 = user0082;
    }

    public void say(){
        System.out.println("包含用户："+user0082.getName());
    }
}
