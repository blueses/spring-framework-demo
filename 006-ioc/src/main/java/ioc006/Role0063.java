package ioc006;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Role0063 {

    private User006 user;

    @Autowired
    public void setUser(User006 user) {
        this.user = user;
    }
}
