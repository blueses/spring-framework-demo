package ioc006;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Role0061 {

    @Autowired(required = false)
    private User006 user;
}
