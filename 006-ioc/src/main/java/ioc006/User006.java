package ioc006;

import org.springframework.stereotype.Component;

@Component
public class User006 {
    private int id = 6;
    private String name = "Jac006";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
