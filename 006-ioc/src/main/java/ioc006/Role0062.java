package ioc006;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Role0062 {

    private User006 user;

    @Autowired
    public Role0062(User006 user) {
        this.user = user;
    }
}
