package ioc0054;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"ioc0051","ioc0052"})
public class Ioc0054Config {
}
