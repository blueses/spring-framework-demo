package ioc005;

import org.springframework.stereotype.Component;

@Component("helloUser005")
public class User005 {
    private int id = 5;
    private String name = "Jac005";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
