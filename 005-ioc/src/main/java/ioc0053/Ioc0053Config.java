package ioc0053;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ioc0051")
//@ComponentScan(basePackages = "ioc0051")
public class Ioc0053Config {
}
