package ioc0055;

import ioc0051.Ioc0051Package;
import ioc0052.Ioc0052Package;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = {Ioc0051Package.class, Ioc0052Package.class})
public class Ioc0055Config {
}
