package ioc015;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class User015 {

    private String id;
    private Nums015 nums015;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Nums015 getNums015() {
        return nums015;
    }

    @Autowired
    public void setNums015(Nums015 nums015) {
        this.nums015 = nums015;
    }
}
