package ioc015;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
public class One015 implements Nums015 {
    @Override
    public void say() {
        System.out.println("111");
    }
}
