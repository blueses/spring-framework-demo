package ioc015;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Ioc015Config.class})
public class Ioc015Test {

    @Autowired
    private User015 user015;


    @Test
    public void testUser(){
        user015.getNums015().say();
    }

}
