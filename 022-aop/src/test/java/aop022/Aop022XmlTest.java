package aop022;

import aop022.service.Aop022Service;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-aop022.xml")
public class Aop022XmlTest {

    @Autowired
    private Aop022Service aopXmlService;


    @Test
    public void testHello() {
        aopXmlService.hello();
    }


    @Test
    public void testCount() {
        aopXmlService.xmlCount(88);
    }
}


