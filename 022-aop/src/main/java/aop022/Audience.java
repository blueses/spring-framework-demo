package aop022;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @author guo
 */
public class Audience {

    public void before() {
        System.out.println(" before  =============");
    }

    public void after() {
        System.out.println(" after  =============");
    }

    public void around(ProceedingJoinPoint point) {
        try {
            System.out.println("around before xml  .......");
            point.proceed();
            System.out.println("around after xml .......");
        } catch (Throwable e) {
            System.out.println("around after throw exception xml .......");
            e.printStackTrace();
        }
    }


    public void afterReturn(int num) {
        System.out.println("after return count xml num=[" + num + "] .......");
    }
}
