package ioc019;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-ioc019.xml"})
//@ContextConfiguration(classes = {Ioc019Config.class})
public class Ioc019Test {

    @Autowired
    private Role019 role019;

//    @Autowired
//    private User019 user019;

    @Test
    public void testUser(){
        role019.say();

        //user019.say();
    }

}
