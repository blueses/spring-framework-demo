package ioc019;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:app.properties")
public class Ioc019Config {

    @Autowired
    private Environment environment;

    @Value("${app.user.age}")
    private int age;

    @Bean
    public User019 user019(@Value("${app.user.name}") String name){

        User019 user019 = new User019();
        user019.setId(environment.getProperty("app.user.id"));
        //user019.setName(environment.getProperty("app.user.name"));

        user019.setName(name);

        //user019.setAge(environment.getProperty("app.user.age", Integer.class));

//        if (environment.containsProperty("app.user.age")) {
//
//            user019.setAge(environment.getRequiredProperty("app.user.age", Integer.class));
//        }

        user019.setAge(age);

        return user019;
    }
}
