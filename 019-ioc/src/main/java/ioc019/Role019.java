package ioc019;

public class Role019 {

    private String name;

    public Role019(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void say(){
        System.out.println("name:"+name);
    }
}
