package ioc004;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-ioc004.xml"})
public class User004Test {

    @Autowired
    private User004 user;

    @Test
    public void testUser(){
        System.out.println(user.getId());
        System.out.println(user.getName());
    }

}
