package ioc004;

import org.springframework.stereotype.Component;

@Component
public class User004 {
    private int id = 4;
    private String name = "Jack004";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
