package ioc0093;

import ioc0091.User0091;
import ioc0092.User0092;
import ioc0094.User0094;

public class Role0093 {
    private User0091 user0091;

    private User0092 user0092;

    private User0094 user0094;

    public User0091 getUser0091() {
        return user0091;
    }

    public void setUser0091(User0091 user0091) {
        this.user0091 = user0091;
    }

    public User0092 getUser0092() {
        return user0092;
    }

    public void setUser0092(User0092 user0092) {
        this.user0092 = user0092;
    }

    public User0094 getUser0094() {
        return user0094;
    }

    public void setUser0094(User0094 user0094) {
        this.user0094 = user0094;
    }
}
