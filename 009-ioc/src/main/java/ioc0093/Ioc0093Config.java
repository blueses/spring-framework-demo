package ioc0093;

import ioc0091.Ioc0091Config;
import ioc0091.User0091;
import ioc0092.Ioc0092Config;
import ioc0092.User0092;
import ioc0094.User0094;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

@Configuration
@Import({Ioc0091Config.class, Ioc0092Config.class})
@ImportResource("classpath:spring-ioc009.xml")
public class Ioc0093Config {

    @Bean
    public Role0093 role0093(User0091 user0091, User0092 user0092, User0094 user0094){
        Role0093 role0093 = new Role0093();
        role0093.setUser0091(user0091);
        role0093.setUser0092(user0092);
        role0093.setUser0094(user0094);
        return role0093;
    }

}
