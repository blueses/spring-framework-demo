package ioc0091;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Ioc0091Config {

    @Bean
    public User0091 user0091(){
        return new User0091("0091","Jack0091");
    }
}
