package ioc0092;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Ioc0092Config {

    @Bean
    public User0092 user0092(){
        return new User0092("0092","Jack0092");
    }

}
