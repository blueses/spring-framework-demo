package ioc011;

public class UserTest011 {

    private String id = "011";
    private String name = "test011";

    public UserTest011() {
    }

    public UserTest011(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
