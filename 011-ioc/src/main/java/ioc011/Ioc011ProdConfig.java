package ioc011;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class Ioc011ProdConfig {

    @Bean
    @Profile("test")
    public UserProd0111 userProd0111(){
        return new UserProd0111();
    }

    @Bean
    @Profile("prod")
    public UserProd0112 userProd0112(){
        return new UserProd0112();
    }
}
