package ioc011;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("test")
@Configuration
public class Ioc011TestConfig {

    @Bean
    public UserTest011 userTest011(){
        return new UserTest011();
    }
}
