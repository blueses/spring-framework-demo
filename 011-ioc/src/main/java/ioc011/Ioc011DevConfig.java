package ioc011;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("dev")
@Configuration
public class Ioc011DevConfig {

    @Bean
    public UserDev011 userDev011(){
        return new UserDev011();
    }
}

