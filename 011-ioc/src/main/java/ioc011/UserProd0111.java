package ioc011;

public class UserProd0111 {

    private String id = "0111";
    private String name = "prod0111";

    public UserProd0111() {
    }

    public UserProd0111(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
