package ioc014;

import org.springframework.stereotype.Component;

@Component
public class Two014 implements Nums014 {
    @Override
    public void say() {
        System.out.println("222");
    }
}
