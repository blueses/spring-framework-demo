package ioc014;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class User014 {

    private String id;
    private Nums014 nums014;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Nums014 getNums014() {
        return nums014;
    }

    @Autowired
    public void setNums014(Nums014 nums014) {
        this.nums014 = nums014;
    }
}
