package ioc014;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Ioc014Config.class})
public class Ioc014Test {

    @Autowired
    private User014 user014;


    /** 因为有歧义性，所以会报错 */
    @Test
    public void testUser(){
        user014.getNums014().say();
    }

}
