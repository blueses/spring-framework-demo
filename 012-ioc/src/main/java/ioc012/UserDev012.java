package ioc012;

public class UserDev012 {

    private String id = "012";
    private String name = "dev012";

    public UserDev012() {
    }

    public UserDev012(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
