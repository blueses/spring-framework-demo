package ioc012;

public class UserProd0122 {

    private String id = "0122";
    private String name = "prod0122";

    public UserProd0122() {
    }

    public UserProd0122(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
