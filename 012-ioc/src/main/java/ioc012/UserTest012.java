package ioc012;

public class UserTest012 {

    private String id = "012";
    private String name = "test012";

    public UserTest012() {
    }

    public UserTest012(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
