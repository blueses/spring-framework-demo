package ioc012;

import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-iocdev0121.xml","classpath:spring-iocprod0122.xml","classpath:spring-ioctest0122.xml"})
@ActiveProfiles("dev")
public class Ioc012Test {
}
