package ioc017;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {Ioc017Config.class})
public class Ioc017Test {

    @Autowired
    private User017 user017;


    @Test
    public void testUser(){
        user017.getNums017().say();
    }

}
