package ioc017;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class User017 {

    private String id;
    private Nums017 nums017;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Nums017 getNums017() {
        return nums017;
    }

    @Autowired
    @Nums
    @One
    public void setNums017(Nums017 nums017) {
        this.nums017 = nums017;
    }
}
